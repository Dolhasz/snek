import sys, pygame
import numpy as np
from enum import Enum

class Dirs:
    UP = [0, -1]
    DOWN = [0, 1]
    LEFT = [-1, 0]
    RIGHT = [1, 0]


class Food:
    def __init__(self, surface):
        self.surface = surface
        self.position = [np.random.randint(self.surface.get_size()[0]), np.random.randint(self.surface.get_size()[1])]
        self.rect = pygame.Rect(*self.position, 10, 10)

    def respawn(self):
        self.rect.x = np.random.randint(self.surface.get_size()[0])
        self.rect.y = np.random.randint(self.surface.get_size()[1])


class Snek: 
    def __init__(self, surface, position, tail_length=1, height=10, width=10):
        self.rect = pygame.Rect(*position, 10, 10)
        self.current_dir = [0, 0]
        self.tail = []
        self.prev_positions = []
        self.prev_direction = None
        self.surface = surface

    def move(self, direction=None):
        # TODO: Make sure snek does not leave the map
        if direction is None:
            direction = self.current_dir
        self.rect.x += direction[0]
        self.rect.y += direction[1]

        if self.rect.x > self.surface.get_size()[0]:
            self.rect.x = 0
        elif self.rect.x < 0:
            self.rect.x = self.surface.get_size()[0]

        if self.rect.y > self.surface.get_size()[1]:
            self.rect.y = 0
        elif self.rect.y < 0:
            self.rect.y = self.surface.get_size()[1]


        tail_length = len(self.tail)
        if tail_length > 0:
            for idx, tail_obj in enumerate(self.tail):
                tail_obj, rgb = tail_obj
                tail_obj.x = self.prev_positions[-(idx+1) * 11][0]
                tail_obj.y = self.prev_positions[-(idx+1) * 11][1]
                
        self.prev_positions.append([self.rect.x, self.rect.y])

    def move_up(self):
        self.move(Dirs.UP)
        self.current_dir = Dirs.UP

    def move_down(self):
        self.move(Dirs.DOWN)
        self.current_dir = Dirs.DOWN

    def move_left(self):
        self.move(Dirs.LEFT)
        self.current_dir = Dirs.LEFT

    def move_right(self):
        self.move(Dirs.RIGHT)
        self.current_dir = Dirs.RIGHT

    def add_tail(self, n=1):
        self.tail.append(
            [
                pygame.Rect(*self.prev_positions[-((len(self.tail) + 1) * 11)], 10, 10),
                [np.random.randint(256),np.random.randint(256),np.random.randint(256)]
            ]
            )


class Game:
    def __init__(self, w=1280, h=720):
        pygame.init()
        self.clock = pygame.time.Clock()
        self.score = 0
        self.size = self.width, self.height = w, h
        self.screen = pygame.display.set_mode(self.size)
        self.surface = pygame.Surface(self.size)
        self.snek = Snek(self.surface, (0, 0))
        self.food = Food(self.surface)

    def run(self, fps=240):
        while True:
            # Event loop
            for event in pygame.event.get():
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        if self.snek.current_dir != Dirs.RIGHT:
                            self.snek.move_left()

                    if event.key == pygame.K_RIGHT:
                        if self.snek.current_dir != Dirs.LEFT:
                            self.snek.move_right()

                    if event.key == pygame.K_UP:
                        if self.snek.current_dir != Dirs.DOWN:
                            self.snek.move_up()

                    if event.key == pygame.K_DOWN:
                        if self.snek.current_dir != Dirs.UP:
                            self.snek.move_down()

                if event.type == pygame.QUIT: sys.exit()
            self.snek.move()

            if self.snek.rect.colliderect(self.food.rect):
                self.snek.add_tail()
                self.food.respawn()
                self.score += 1

            if any([self.snek.rect.colliderect(t[0]) for t in self.snek.tail[5:]]):
                sys.exit()

            font = pygame.font.SysFont("comicsansms", 72)
            text = font.render(f'Score: {self.score}', True, (0, 128, 0))

            # Render
            self.screen.fill((0,0,0))
            self.screen.blit(text, (self.width - 500, self.height - 100))
            pygame.draw.rect(self.screen, (255, 255, 255), self.snek.rect)
            pygame.draw.rect(self.screen, (255, 0, 0), self.food.rect)
            for idx, tail_element in enumerate(self.snek.tail):  # TODO: Make every tail bit have a different colour
                tail_element, rgb = tail_element
                pygame.draw.rect(self.screen, rgb, tail_element)

            pygame.display.flip()
            self.clock.tick(240+(10*self.score))


if __name__ == "__main__":
    game = Game()
    game.run()

